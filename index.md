<title>Marlon's Homepage</title>

![Header image](/header.jpg)

# Marlon's Homepage

Damit ich mir das nicht merken muss: [Wunschzettel](wunschzettel.html)

## [Berechnung der vorausichtlichen Arbeitszeit während eines Semesters](https://worktimescalculator.regenhardt.dev)

![Work times calculator preview image](/worktimescalculator.png)

## [Page Monitor](https://gitlab.com/Regenhardt/page-monitor):

Prüft alle paar Minuten auf das Vorkommen eines bestimmten Textes auf Websites und meldet ein solches Vorkommen über Slack.  
Ursprünglich gebaut um die Verfügbarkeit der Xbox Series X Halo Edition mitzukriegen.

Aktuell kann er tifu.info prüfen, ob ein gegebener Spieler gerade dran ist.

## [Spielplangenerator](https://gitlab.com/Regenhardt/kickern)

Generiert einen Spielplan für ein Ligaspiel der Hambuger Tischfußballliga. 

Konfigurieren der Spieler:

![Webseite mit einer Tabelle konfigurierter Spieler mit Einstellungen zu Einspielmoduspräferenz, präferierter Position, und ob nur 1. Halbzeit](/spg-config.png)

Generierter Spielplan:

![Website mit einer Tabelle, die links die zu spielenden Spiele und rechts die dafür ausgewählten Spieler zeigt](/spg-plan.png)
