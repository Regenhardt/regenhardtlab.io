<link rel="stylesheet" type="text/css" href="wunschzettel.css">
<title>Marlon's Wunschzettel</title>

# Marlons Wunschzettel

Oder: Dinge die ich mir aus unterschiedlichen Gründen noch nicht selbst gekauft habe.

## Rasierer

Ein elektrischer Rasierer wäre wohl mal gut, da ich immer zu faul bin mich nass zu rasieren und der Trimmer natürlich nicht gut rasiert.

## Quest 3 Zubehör

Für mein neue Quest 3 kann ich natürlich etwas Zubehör gebrauchen.

### Head Strap

Für angenehmeres Tragen und vielleicht sogar zusätzliche Akkulaufzeit.

![Head Strap](https://www.bobovr.com/cdn/shop/files/20231120-180812.jpg)

### Tragetasche

Zum Lagern, mitnehmen und sie drin aufzuladen.

[![](https://m.media-amazon.com/images/I/91fjVO6HgjL._AC_SL1500_.jpg)](https://www.amazon.de/DEVASO-Tragetasche-Kompatibel-Controllern-Aufbewahrung/dp/B0CMWJ1KKW)

## VR Spiele

Es gibt natürlich auch diverse Spiele in VR, die ich gerne mal spielen würde, in unbestimmter Reihenfolge (Preise sind nicht aktuell, da es nur ein Screenshot ist):

<picture>
    <source media="(min-width: 1080px)" srcset="wishlist_4-column.png">
    <source media="(min-width: 768px)" srcset="wishlist_2-column.png">
    <img src="wishlist_1-column.png">
</picture>
